% %Problem 1 gaussian blurring and unblurring
clear; 
close all;
%testing 1(i)
im1 = im2single(imread('data/pattern.tif'));
im2 = gaussianBlur(im1, 10);
figure();
subplot(1,2,1);
imshow(im1, []);
subplot(1,2,2)
imshow(im2, []);

%testing 1(ii)
im1 = im2single(imread('data/pattern.tif'));
im2 = blurOrSharpen(im1, -1, 10);
figure();
subplot(2,2,1);
imshow(im1, []), title('Image');
subplot(2,2,2)
imshow(im2, []), title('Blurred Image');
 
im3 = blurOrSharpen(im1, 1, 10);
subplot(2,2,3);
imshow(im3, []), title('Sharpened Image');
subplot(2,2,4)
im4 = blurOrSharpen(im2, 1, 10);
imshow(im4, []), title('LPF & HPF');

% %testing 1(iiia)
im1 = im2single(imread('data/brain.tif'));
im1 = gaussianBlur(im1, 2);
[im_out, mses, mseResiduals, iter] = gaussianUnblur(im1, 2, 500, .0001);
X = 1:iter;

figure();
subplot(4,1,1)
imshow(im1, []), title('Image');
subplot(4,1,2);
imshow(im1, []), title('Blurred Image');
subplot(4,1,3);
plot(X, mses, X, mseResiduals),legend('MSE','MSE Residuals'),title('MSE and MSE Residual');
subplot(4,1,4);
imshow(im_out, []), title('Unblurred Image');

% %testing 1(iiib)
im1 = im2single(imread('data/brain.tif'));
im1 = gaussianBlur(im1, 2);
im1 = single(imnoise(uint8(im1), 'gaussian'));
[im_out, mses, mseResiduals, iter] = gaussianUnblur(im1, 2, 500, .0001);
X = 1:iter;

figure();
subplot(4,1,1)
imshow(im1, []), title('Noisy Image');
subplot(4,1,2);
imshow(im1, []), title('Blurred Noisy Image');
subplot(4,1,3);
plot(X, mses, X, mseResiduals),legend('MSE','MSE Residuals'),title('MSE and MSE Residual w/ Noise ');
subplot(4,1,4);
imshow(im_out, []), title('Unblurred Noisy Image');
