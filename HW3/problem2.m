% Read in the image Car.tif, pad the image to 512x512, 
% and display the 2D-FFT log magnitude
% (after moving the DC component to the center with fftshift):
% imagesc(-256:255,-256:255,log(abs(imFFT))); colorbar;
% xlabel(?u?); ylabel(?v?);
close all;
clear;

img = im2single(imread('data/street.png'));
imgX = 512;
imgY = 512;


%pad image to 512x512 & compute 2D-FFT
F = fft2(img, imgX, imgY); 

F = fftshift(F);

figure();
subplot(3,1,1);
imagesc(-256:255,-256:255,log(abs(F))), title('2D-DFT') ,colorbar;
xlabel('u'); ylabel('v'); title('Centered 2D-DFT');
hold on;
%Target Pair 1
uk1 = 0;
vk1 = 165;

%Target Pair 2
uk2 = 165;
vk2 = 0;

%Confirm Notch Targets
plot(uk1,vk1,'o');
plot(uk2,vk2,'o');
plot(-uk1,-vk1,'o');
plot(-uk2,-vk2,'o');


%Create Notch Filter

[u,v] = meshgrid(-256:255);
n = 2;
D0 = 8;
[filt_a1, filt_b1] = makeSymmetricNotchFilters(uk1,vk1,u,v,n,D0);
[filt_a2, filt_b2] = makeSymmetricNotchFilters(uk2,vk2,u,v,n,D0);
% [filt_a3, filt_b3] = makeSymmetricNotchFilters(uk3,vk3,u,v,n,D0);
% [filt_a4, filt_b4] = makeSymmetricNotchFilters(uk4,vk4,u,v,n,D0);

%Create filter
F_H = filt_a1.*filt_b1.*filt_a2.*filt_b2;
% .*filt_a3.*filt_b3.*filt_a4.*filt_b4;
subplot(3,1,2);
imagesc(-256:255,-256:255,log(abs(F_H))); colorbar;
xlabel('u'); ylabel('v'); title('Filter');


%apply filters to image
F_filtered = F.*F_H;
subplot(3,1,3);
imagesc(-256:255,-256:255,log(abs(F_filtered))); colorbar;
xlabel('u'); ylabel('v'); title('Filtered 2D-DFT');

%apply IFFT
F_filtered = ifftshift(F_filtered);
img_filtered = abs(ifft2(F_filtered));

figure();
subplot(2,1,1);
imshow(img, []),title('Noisy Image'), colorbar;
subplot(2,1,2);
imshow(img_filtered(1:size(img,1),1:size(img,2)), []), title('Filtered Image'), colorbar;

function [filter1, filter2] = makeSymmetricNotchFilters(uk, vk, u, v, n , D0)
    D_K = ((u - uk).^2 + (v - vk).^2).^.5;
    D_negK = ((u + uk).^2 + (v + vk).^2).^.5;
    filt_p = 1./(1+(D0./D_K).^(2*n));
    filt_n = 1./(1+(D0./D_negK).^(2*n));
    filter1 = filt_p.*filt_n;
    
    D_K = ((u + uk).^2 + (v + vk).^2).^.5;
    D_negK = ((u - uk).^2 + (v - vk).^2).^.5;
    filt_p = 1./(1+(D0./D_K).^(2*n));
    filt_n = 1./(1+(D0./D_negK).^(2*n));
    filter2 = filt_p.*filt_n;
end