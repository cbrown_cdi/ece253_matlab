function [im_out] = gaussianBlur(im_in,sigma)
%UNTITLED2 Summary of this function goes here
%   Apply a gaussian filter with standard deviation sigma. Padding is
%   modified to the default method used by imfilter which is symmetric.

im_out = imgaussfilt(im_in, sigma, 'padding', 'symmetric');
end

    