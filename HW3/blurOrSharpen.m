function [im_out] = blurOrSharpen(im_in, w, sigma)
%BLURORSHARPEN Summary of this function goes here
%   Implementing the following function:
%       I' = (1 + w) � I ? w � I ? G?
    if w > 1 || w < -1 
        error('Error! w must be between -1 and 1');
    end
    %gaussianBlur computes I * G
    im_out = (1 + w) * im_in - (w * gaussianBlur(im_in, sigma));    
end

