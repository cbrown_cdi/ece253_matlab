function [Ik, mses, mseResiduals, iter] = gaussianUnblur(im_in, sigma, max_iter, t)
%GAUSSIANUNBLUR Summary of this function goes here
%   It should implement the following procedure -
% Let I0 be the blurry input image, Ik the corrected image at iteration k, and G? the
% Gaussian filter you have already implemented in 1(i). Iterate the following steps over
% k = 0, 1, 2, . . .
% (a) Compute Ak = Ik ? G?
% (b) Set Bk = I0/Ak
% (c) Compute Ck = Bk ? G?
% (d) Set Ik+1 = Ik � Ck,
% 
% where operators ?, / and � denote convolution, point-wise division and point-wise mul-
% tiplication respectively.
% 
% ? You should run these steps until the image Ik converges, that is, the residual (MSE
% between Ik and Ik+1) from one iteration to the next is very small (less than the user
% supplied threshold t). The maximum iteration count max iter is to bail out just in case
% the process does not converge.

    Ik = im_in;
    iter = 0;
    
    %pre-allocate
    mses = [];
    mseResiduals = [100000];%enter loop

    
   while mseResiduals(end) >= t && iter < max_iter
        iter = iter+1;
        prevIk = Ik;
        Ak = gaussianBlur(Ik, sigma);
        Bk = im_in ./ Ak;
        Ck = gaussianBlur(Bk, sigma);
        Ik = Ik .* Ck;
        
        mseResiduals = [mseResiduals, double(immse(Ik, prevIk))];
        mses = [mses, double(immse(Ik, im_in))];
    end
    mseResiduals = mseResiduals(2:end);
end
