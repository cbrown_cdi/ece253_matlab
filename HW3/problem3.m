clear;
close all;

%Problem 3(i)
% Cross-correlation filter on a simple image
% Read in image Letters.jpg and template LettersTemplate.jpg and convert to
% grayscale, double type. Perform cross-correlation on the image with the template 
% using the convolution operation in spatial domain, and the point-wise 
% multiplication operation in frequency domain.
% 
% Display the resulting filtered images from both methods.
% Note: you may need to shift the resulting image from the frequency domain method.
% The following functions may be useful:
% conv2(), fft2(), ifft2(), circshift().

img = rgb2gray(im2double(imread('data/StopSign.jpg')));
template = rgb2gray(im2double(imread('data/StopSignTemplate.jpg')));

figure();
subplot(3,1,1);
imshow(img), title('Image'), colorbar;

[template_x, template_y] = size(template);

%Spatial domain cross-correlation
%flip template, then convolve 
template_flip = flip(template, 1);
img_xcorr = conv2(template_flip, img);
subplot(3,1,2);
imshow(img_xcorr, []), title('Spatial xcorr'),colorbar;
[~,snd] = max(img_xcorr(:));
[ij,ji] = ind2sub(size(img_xcorr),snd);
output = ['Spatial xcorr peak: ', num2str(ji), ', ', num2str(ij)];
disp(output);



%Frequency domain cross-correlation
[m, n] = size(img);
[tm, tn] = size(template);
f_img = fft2(img, 2*m, 2*n);
f_template = fft2(template, 2*m, 2*n);
f_img_f_xcorr = f_img.*conj(f_template);
img_f_xcorr = ifft2(f_img_f_xcorr);
img_f_xcorr = circshift(img_f_xcorr, [tm-1 tn-1]);
img_f_xcorr = img_f_xcorr(1:m, 1:n);
subplot(3,1,3);
imshow(img_f_xcorr, []), title('Frequency xcorr'), colorbar;
[~,snd] = max(img_f_xcorr(:));
[ij,ji] = ind2sub(size(img_f_xcorr),snd);
output = ['Freq xcorr peak: ', num2str(ji), ', ', num2str(ij)];
disp(output);

% figure();
norm_corr = xcorr2(img, template);
% imshow(norm_corr, []);
[~,snd] = max(norm_corr(:));
[ij,ji] = ind2sub(size(norm_corr),snd);
output = ['Matlab xcorr peak: ', num2str(ji), ', ', num2str(ij)];
disp(output);

% Problem 3(iii)
% Normalized cross-correlation
% Apply normalized cross-correlation on the stop sign image using the 
% corresponding template and display the resulting image with a colorbar. 
% Also display the original image with a with a rectangular box (the same 
% size as the template) at the location with the highest normalized
% cross-correlation score. The following functions may be useful:
% normxcorr2(), rectangle() or insertShape().

im = rgb2gray(im2single(imread('data/StopSign.jpg')));
temp2 = rgb2gray(im2single(imread('data/StopSignTemplate.jpg')));
[targetX, targetY] = size(temp2);
figure();
norm_corr = normxcorr2(temp2, im);
subplot(2,1,1);
imshow(norm_corr, []), colorbar, title('Normalized xcorr freq domain');
[~,snd] = max(norm_corr(:));
[ij,ji] = ind2sub(size(norm_corr),snd);
subplot(2,1,2);
imshow(im, []), colorbar, title('Normalized xcorr spatial domain');
hold on;
rectangle('Position',[(ji - targetY),(ij - targetX),...
    targetY, targetX],'LineWidth',1, 'EdgeColor', 'r')
output = ['Norm xcorr peak: ', num2str(ji), ', ', num2str(ij)];
disp(output);
