function [im_seg] = mapValues(im,idx)
%MAPVALUES Summary of this function goes here
% This function takes in the cluster membership vector idx (N �1), and 
% returns the segmented image im seg as the output. Each pixel in the 
% segmented image must have the RGB value of the cluster center to which 
% it belongs. You may use loops for this part.
% Functions that you may find useful : mean(), cat().

%Get number of clusters
kClusters = length(unique(idx));
[~, ~, featureVectors] = size(im);
clusterColors = zeros(kClusters, featureVectors);

centers = zeros(kClusters, 2); %create array to hold center of each region
[im_m, im_n, ~] = size(im);
im_seg = zeros(size(im));
idx_mask = reshape(idx, [im_m, im_n]);
idx_mask = cat(3, idx_mask, idx_mask, idx_mask);%create RBG mask

figure();

%calcluate center of each cluster
for i = 1:kClusters
%     idx_mask = zeros(size(idx_mask));
    A = (idx_mask() == i);% points in this cluster
%     disp('A count: ');
%     disp(sum(A(:) == 1));
    
    %Need average of X & Y for this cluster
    maskedImage = A.*im;%apply mask
    [centerX, centerY] = find(maskedImage(:,:,1));%feature space dim is irrelevent. Just need location of center
    centers(i,1) = round(mean(centerX));% update custer centers, round to nearest pixel
    centers(i,2) = round(mean(centerY));
    clusterColors(i,:) = squeeze(im(centers(i,1),centers(i,2),:)); %get RGB at point
    
    red_mask = A(:,:,1).*clusterColors(i,1);
    green_mask = A(:,:,2).*clusterColors(i,2);
    blue_mask = A(:,:,3).*clusterColors(i,3);
    
    RGB_mask =  cat(3, red_mask, green_mask, blue_mask);
%     disp('Unique values in mask:');
%     disp(length(unique(RGB_mask)));
%     maskedRgbImage = bsxfun(@times, im, cast(RGB_mask,class(im)));
    im_seg = im_seg + RGB_mask;
    subplot(4,2,i);
    imshow(RGB_mask);
    hold on;
    plot(centers(i,2),centers(i,1),'x','LineWidth',2,'Color','red');
    hold off;
end

figure();
imshowpair(im, im_seg, 'montage');
hold on;
% for n= 1:kClusters
%     plot(centers(n,2),centers(n,1),'x','LineWidth',2,'Color','red');
% end
% hold off;
end

