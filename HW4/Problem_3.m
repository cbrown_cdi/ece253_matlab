% In this problem, we shall implement a K-Means based segmentation algorithm from scratch. To
% do this, you are required to implement the following three functions -
% ? features = createDataset(im) : This function takes in an RGB image as input, and returns a
% dataset of features which are to be clustered. The output features is an N �M matrix where
% N is the number of pixels in the image im, and M = 3 (to store the RGB value of each pixel).
% You may not use a loop for this part.
% ? [idx, centers] = kMeansCluster(features, centers) : This function is intended to perform
% K-Means based clustering on the dataset features (of size N � M). Each row in features
% represents a data point, and each column represents a feature. centers is a k � M matrix,
% where each row is the initial value of a cluster center. The output idx is an N � 1 vector that
% stores the final cluster membership (? 1, 2, � � � , k) of each data point. The output centers are
% the final cluster centers after K-Means. Note that you may need to set a maximum iteration
% count to exit K-Means in case the algorithm fails to converge. You may use loops in this
% function.
% Functions you may find useful : pdist2(), isequal().
% ? im seg = mapValues(im, idx) : This function takes in the cluster membership vector idx
% (N �1), and returns the segmented image im seg as the output. Each pixel in the segmented
% image must have the RGB value of the cluster center to which it belongs. You may use loops
% for this part.
% Functions that you may find useful : mean(), cat().

close all;
clear;

im = im2double(imread('data/white-tower.png'));
features = createDataset(im);
%define starting centers
rng(5);
nclusters = 7;
id = randi(size(features, 1), 1, nclusters);
centers = features(id, :);
%perform clustering
[idx, finalCenters] = kMeansCluster2(features, centers);
disp('Final Centers: ');
disp(finalCenters);
im_seg = mapValues(im, idx);


figure();
imshow(im, []), title('Original Image');
figure();
imshow(im_seg, []), title('kMeans Segemented Image');

