%Problem 2
%Hough Transform
close all;
clear;

% Creating test image
% im = zeros(11,11);
% im(1,1) = 1;
% im(1,end) = 1;
% im(end,1) = 1;
% im(end,end) = 1;
% im(ceil(end/2),ceil(end/2)) = 1;



% %load lane image
im_orig = imread('data/lane.png');
figure(), imshow(im_orig), title('Image');
im = rgb2gray(im_orig);
im = edge(im, 'sobel');
figure(), imshow(im), title('Binary Image');


H = myHough(im);
H(H(:) < 0.75*max(H(:))) = 0;
%H(H(:) < 2) = 0;
% figure();
% imshow(H), title('Thresholded HoughSpace');

%Convert to image space and plot lines   
[im_height, im_width] = size(im);
rho_maximum = ceil(sqrt(im_height.^2 + im_width.^2));

[rho, theta] = find(H); %the non-zero matrix elements are now the strong line theta-rho coordinates
lines = initializeEmptyLinesStructArray();
numlines = 0;
for n = 1:length(theta)
    disp(['N = ' num2str(n) '. Rho = ' num2str(rho(n)) '. Theta = ' num2str(theta(n))]);
    if theta(n) >= 144 && theta(n) <= 166 %restriction for lanes only
        if theta(n) >= 45 && theta(n) <= 135 
        %y = (r - x cos(t)) / sin(t)  
            x1 = 0;
            y1 = round(((rho(n) - rho_maximum) - (x1 * cosd(theta(n)))) / sind(theta(n)));  
            x2 = im_width;
            y2 = round(((rho(n) - rho_maximum) - (x2 * cosd(theta(n)))) / sind(theta(n))); 
        else    
        %x = (rho - y sin(theta)) / cos(theta);  
            y1 = 0;        
            x1 = round(((rho(n) - rho_maximum) - (y1 * sind(theta(n)))) / cosd(theta(n)));  
            y2 = im_height;
            x2 = round(((rho(n) - rho_maximum) - (y2 * sind(theta(n)))) / cosd(theta(n)));  

        end
    p1 = [abs(y1), abs(x1)];
    p2 = [abs(y2), abs(x2)];
    numlines = numlines + 1;
    lines(numlines).point1 = p1;
    lines(numlines).point2 = p2;
    lines(numlines).theta = theta(n);
    lines(numlines).rho = rho(n);
    end
end
       
%plot lines    
figure(), imshow(im_orig,'InitialMagnification',1000), title('Image with lines'), hold on
max_len = 0;
for k = 1:length(lines)
    xy = [lines(k).point1; lines(k).point2];
    plot(xy(:,1),xy(:,2),'LineWidth',2,'Color','green');
end

function lines = initializeEmptyLinesStructArray()
    tmp.point1 = [0,0];
    tmp.point2 = [0,0];
    tmp.theta  = 0;
    tmp.rho    = 0;
    lines = repmat(tmp,1,0);    
end

function [ HS, theta, rho_range ] = myHough(im)
    % Initializing other parameters
    theta = -90:1:90;%1 degree increments
    D = ceil(sqrt(size(im,1).^2 + size(im,2).^2));%calculate image diagonal
    HS = zeros(ceil(2.*D),numel(theta));%preallocate array
    rho_range = floor(-D):1:ceil(D);
    [y,x] = find(im);
    y = y - 1;
    x = x - 1;
    rho = cell(1,numel(x));
    % Calculating the Hough Transform
    for i = 1: numel(x)
        rho{i} = x(i).*cosd(theta) + y(i).*sind(theta); % [-sqrt(2),sqrt(2)]*D rho interval
    end

    % Creating the Hough Space as an Image
    for i = 1:numel(x)
        rho{i} = rho{i} + D; % mapping rho from 0 to 2*sqrt(2)
        rho{i} = floor(rho{i}) + 1;
        for j = 1:numel(rho{i})
            HS(rho{i}(j),j) = HS(rho{i}(j),j) + 1; 
        end
    end
    figure();
    imagesc(theta,rho_range,HS), colorbar;
    xlabel('Theta'), ylabel('Rho'), title('Hough Space');
end

