function [imgOut] = CannyEdgeDetector(img,te)
%CANNY_EDGE_DETECTOR the following steps are used to located the edges
%   1. Smoothing via gaussian filter with sigma = 1.4
%   2. Finding Gradients using sobel operators
%   3. Non-maximum suppresison, reduces edge width
%   4. Thresholding, allows filtering based on edge strength

%Convert image to grayscale
    img = rgb2gray(img);
%     figure();
%1. Gaussian Filtering
    sigma = 1.4;
    img = imgaussfilt(img, sigma);
%2. Sobel Operations. Find gradient and direction
    [Gmag,Gdir] = imgradient(img);
    subplot(3,1,1);
    imshow(Gmag, []), title('Filtered Gradient Magnitude');
    %Round to nearest 45 degree increment 
    Gdir_45 = floor(Gdir/45)*45;
%3. Non-Maximum suppression.
    [m,n]=size(Gdir_45);
    mask=uint8(zeros(size(Gdir_45)));
    for i=2:m-1
        for j=2:n-1
            if Gmag(i,j) ~= 0 %only need to check things with a value
               angle=abs(Gdir_45(i,j));  %since we're checking along positive and negative gradient directions we can use the absolute calue
                if angle == 0 || angle == 180  %horizontal
                    if (Gmag(i,j) > Gmag(i,j+1)) && (Gmag(i,j) > Gmag(i, j-1))
                        mask(i,j)=1;
                    end
                elseif angle == 45 || angle == 135 %45, 135
                     if (Gmag(i,j)) > Gmag(i+1,j+1)|| (Gmag(i,j) > Gmag(i-1,j-1))
                        mask(i,j)=1;
                     end
                else %90
                    if (Gmag(i,j) > Gmag(i+1,j)) && (Gmag(i,j) > Gmag(i-1,j))
                        mask(i,j)=1;
                    end
                end
            end
        end 
    end
    %Multiply mask with gradient
     Gmag = double(mask) .* Gmag;
    subplot(3,1,2);
    imshow(Gmag, []), title('Gradient after NMS');
    
%4. Threshold image
    imgOut = imbinarize(Gmag, te);
    subplot(3,1,3);
    imshow(imgOut, []), title('Thresholded Image');
end

