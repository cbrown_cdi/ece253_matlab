function [idx, centers] = kMeansCluster(features, centers)
%KMEANSCLUSTER Summary of this function goes here
% This function is intended to perform K-Means based clustering on the 
% dataset features (of size N � M). Each row in features represents a data 
% point, and each column represents a feature. centers is a k � M matrix,
% where each row is the initial value of a cluster center. The output idx 
% is an N � 1 vector that stores the final cluster membership 
% (? 1, 2, � � � , k) of each data point. The output centers are the final 
% cluster centers after K-Means. Note that you may need to set a 
% maximum iteration count to exit K-Means in case the algorithm fails 
% to converge. You may use loops in this function.
% Functions you may find useful : pdist2(), isequal().

% features 3 here RBG:
%1. R G B
%2. R G B
%...
%N. R G B

% Least Squared Euclidian Distance i.e. nearest cluster mean for each color

    [kClusters, ~] = size(centers); %extract number of clusers from centers matrix, kWidth is RGB or 3
    lastCenters = zeros(size(centers));%create matrix with same dims to track previous values

    maxIter = 20; %maximum number if iterations before bailing

    [featureRows, ~, ~] = size(features);
    
    dist_labels = zeros(size(features,1),kClusters+2); % Distances and Labels
    disp(['K-means iterations remaining: ' num2str(maxIter)]);
    averagePercentChange = [];
%   feature_currCluster = [features zeros(size(features,1),1)];

    while (~isequal(centers, lastCenters) && maxIter > 0)
        maxIter = maxIter - 1; % count iteration
        lastCenters = centers;
%   calculate closest centroids to each points
        for row = 1:featureRows%for each row in features
            %find closest centroid and assign point to that centroid
            for k = 1:kClusters
                %dist_labels(row, k) = pdist2(features(row,:), centers(k,:), 'squaredeuclidean');
                dist_labels(row, k) = norm(features(row, :) -  centers(k, :));%calculate distance
            end
            [distance, label] = min(dist_labels(row,1:kClusters));  % 1:Kclusters are Distance from Cluster Centers 1:K 
            dist_labels(row,kClusters+1) = label;% K+1 is Cluster Label
            dist_labels(row,kClusters+2) = distance;% K+2 is Minimum Distance
        end
% Calculate new centroid center
        for i = 1:kClusters
            thisCluster = (dist_labels(:,kClusters+1) == i);% points in this cluster
            centers(i,:) = mean(features(thisCluster,:));% update custer centers
        end
        %calculate center location percent change
%         disp('Centers percent changes:');
        percentChange = 100.*abs((centers - lastCenters)./lastCenters);
%         disp(percentChange);
        %calculate average percent change
        disp('Average Percent Change:');
        disp(mean2(percentChange));
        averagePercentChange = [averagePercentChange, mean2(percentChange)];
        disp(['K-means iterations remaining: ' num2str(maxIter)]);

    end
    idx = dist_labels(:, kClusters +1);
    
figure();
plot(averagePercentChange), title('Average Percent Change');
xlabel('Iterations');
ylabel('Percent Change');
grid on;
end 

