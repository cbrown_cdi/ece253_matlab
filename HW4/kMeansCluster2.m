function [idx, centers] = kMeansCluster2(features,centers)
%Calculates kMeans clusters without looping over pixels. Also calculates
%and tracks the average percent change of all the centers. Exits at
%when all centers have stopped moving or once maximum iteration number is
%met.
    [row,~] = size(features);
    [center_number,~] = size(centers);
    lastCenters = zeros(size(centers));%create matrix with same dims to track previous values

    idx = zeros(row,1);
    maxIter = 100;
    averagePercentChange = zeros(size(maxIter));
    while (~isequal(centers, lastCenters) && maxIter > 0)
        lastCenters = centers;
        %Calculate nearest center for each point
         center_distances = pdist2(features(:,:),centers(:,:));
        [~,nearest_center] = min(center_distances,[],2);
        idx = nearest_center;

        %Re-calculate centers
        centers = zeros(center_number,3);

        for i = 1:center_number
            indices = find(idx(:) == i);    %find things from this center
            center_average = features((indices),:); 
            centers(i,:) = mean(center_average);   %update centers average
        end
        maxIter = maxIter - 1; 
        %calculate average percent change
        percentChange = 100.*abs((centers - lastCenters)./lastCenters);
        disp('Average Percent Change:');
        disp(mean2(percentChange));
        disp(['K-means iterations remaining: ' num2str(maxIter)]);
        averagePercentChange = [averagePercentChange, mean2(percentChange)];

    end
    
    figure();
    plot(averagePercentChange), title('Average Percent Change of Cluster Mean');
    xlabel('Iterations');
    ylabel('Percent Change');
    grid on;
end

