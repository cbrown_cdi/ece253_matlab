function [features] = createDataset(im)
%CREATEDATASET Summary of this function goes here
% This function takes in an RGB image as input, and returns a
% dataset of features which are to be clustered. The output features is an N � M matrix where
% N is the number of pixels in the image im, and M = 3 (to store the RGB value of each pixel).
% You may not use a loop for this part.
[im_m, im_n, im_o] = size(im);
features = reshape(im, [im_m*im_n, im_o]);
end

