%clear %probably don't want to clear...slow

image = imread('data/beach.png');

figure();
subplot(2,3,1);
imshow(image, []), title('image');
subplot(2,3,2);
%r33 = AHE(image, 33);
imshow(r33, []), title('winSize = 33');
subplot(2,3,3);
%r65 = AHE(image, 65);
imshow(r65, []), title('winSize = 65');
subplot(2,3,4);
%r129 = AHE(image, 129);
imshow(r129, []), title('winSize = 129');
subplot(2,3,5);
he = histeq(image);
imshow(he, []), title('matlab histogram eq');
subplot(2,3,6);
ahe = adapthisteq(image);
imshow(ahe, []), title('matlab adaptive histogram eq');


