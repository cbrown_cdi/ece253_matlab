clear;
close;

diver = imread('data/diver.tif');
lena = imread('data/lena512.tif');

%compare Lloyd and uniform quantizations over range 1:7 for images
[diver_MSE_LM, diver_MSE_uniform] = compareLloyd_Uniform(diver, 1:7);
[lena_MSE_LM, lena_MSE_uniform] = compareLloyd_Uniform(lena, 1:7);

%create new equalized images
diver_hist = histeq(diver,256);
lena_hist = histeq(lena,256);
%calculate quantizations and MSEs
[diver_eq_MSE_LM, diver_eq_MSE_uniform] = compareLloyd_Uniform(diver_hist, 1:7);
[lena_eq_MSE_LM, lena_eq_MSE_uniform] = compareLloyd_Uniform(lena_hist, 1:7);

figure();
subplot(2,2,1);
plot(1:7, diver_MSE_uniform, 'r-'), title('Diver');
hold on;
plot(1:7, diver_MSE_LM, 'k-');
grid on;
xlabel('Quantization bits');
ylabel('MSE');
legend('Uniform', 'Lloyd-Max');

subplot(2,2,2);
plot(1:7, lena_MSE_uniform, 'r-'), title('Lena512');
hold on;
plot(1:7, lena_MSE_LM, 'k-');
grid on;
xlabel('Quantization bits');
ylabel('MSE');
legend('Uniform', 'Lloyd-Max');

subplot(2,2,3);
plot(1:7, diver_eq_MSE_uniform, 'r-'), title('Diver Equalized');
hold on;
plot(1:7, diver_eq_MSE_LM, 'k-');
grid on;
xlabel('Quantization bits');
ylabel('MSE');
legend('Uniform', 'Lloyd-Max');

subplot(2,2,4);
plot(1:7, lena_eq_MSE_uniform, 'r-'), title('Lena512 Equalized');
hold on;
plot(1:7, lena_eq_MSE_LM, 'k-');
grid on;
xlabel('Quantization bits');
ylabel('MSE');
legend('Uniform', 'Lloyd-Max');

figure();
imshowpair(diver,lena,'montage');
