function [resultImage] = uniformQuant(image,s)
%UNTITLED4 Summary of this function goes here
%   Quantatize image via uniform algorithm

    %check if s is between 7 and 1
    if (s > 7 || s < 1)
        warning('Error! s is not between 1 and 7.\n');
        return;
    elseif size(image, 3) ~= 1
        warning('Error! Image has more than one channel.\n');
        return;
    end

    step = round(255/2^s);

    %partition range, this allows a full scale image to be returned
    range = round(0:step:255);
    if range(end) ~= 255
        range(end+1) = 255;
    end
    %preallocate resultImage
    resultImage = uint8(zeros(size(image)));

    %assign values to pixels
    for i = 1:length(range)-1
        resultImage(image > range(i) & image <= range(i+1))= round((range(i) + range(i+1))/2);
    end
end





