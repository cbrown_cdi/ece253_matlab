function [MSE_LM, MSE_uniform] = compareLloyd_Uniform(img1, range)
%COMPARELLOYD_UNIQUE Summary of this function goes here
%   Compare Lloyd Max Quantization to Uniform Quantiaztion. Return MSE of
%   both types of images for range of bits(range).
    MSE_uniform = zeros(1,7);
    MSE_LM = zeros(1,7);

    for s = range
        imgLM = zeros(size(img1));
        [M,N] = size(img1);
        training_set = double(reshape(img1, N*M, 1));
        [partition, codebook] = lloyds(training_set, 2^s);
        partition = round(partition);
        codebook = round(codebook);

        for i = 1:length(partition)-1
           imgLM(img1 > partition(i) & img1 <= partition(i+1)) = codebook(i+1);
        end
        imgLM(img1 <= partition(1)) = round(codebook(1));
        imgLM(img1 > partition(end)) = round(codebook(end));

        imgLM = uint8(imgLM);

        imgUniform = uniformQuant(img1, s);

        MSE_uniform(s) = sum(sum((double(img1)-double(imgUniform)).^2))/numel(img1);
        MSE_LM(s) = sum(sum((double(img1)-double(imgLM)).^2))/numel(img1);
    end
end

