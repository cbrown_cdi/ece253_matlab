function [result] = AHE(img,winSize)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% Psuedocode
% for each (x,y) in image do
% {
%   rank= 0
%   for each (i,j) in contextual region of (x,y) do
%   {
%       if image[x,y] > image[i,j] then
%       rank = rank + 1
%   }
%       output[x,y] = rank * max_intensity I (# of pixels in contextual region) 
% }


% calculate pad size and add to image
padImg = padarray(img, [winSize, winSize], 'symmetric');
padSize = floor(winSize/2);
output = zeros(size(padImg));

% itterate through rows and columns
for i = padSize + 1 : size(padImg,1) - padSize %columns
    for j = padSize + 1 : size(padImg,2) - padSize %rows
        % each pixel
        rank = 0;
        region = padImg(i - padSize: i + padSize, j - padSize: j+padSize);
        for ii=1:size(region,1) %columns
            for jj=1:size(region,2) %rows
                if padImg(i,j) > region(ii,jj)
                    rank = rank + 1;
                end
            end
        end
        output(i,j) = round(rank*255/(winSize*winSize));
    end
end
result = output(winSize+1:size(img,1)+winSize, winSize+1:size(img,2)+winSize);
end

