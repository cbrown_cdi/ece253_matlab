% For the binary image circles lines.jpg, your aim is to separate out the circles in the image,
% and calculate certain attributes corresponding to these circles.
% 
% ? The first step is to remove the lines from the image. This can be achieved by perform-
% ing the opening operation with an appropriate structuring element. You
% may find the following MATLAB functions to be of use - imopen(), strel().
% ? Once we have a binary image with just the circles, the individual regions need to be
% labeled to represent distinct objects in the image i.e. connected component labeling.
% This can be done in MATLAB using the function bwlabel().
% ? We now have access to individual regions in the image, each of which can be analyzed
% separately. For each labeled circular region, calculate its centroid and area. Tabulate
% these values for each connected component. You may use loops for this part of the
% problem.
clear;
close;

img = imread('data/circles_lines.jpg');
binImg = imbinarize(img(:,:,1));%all three matricies are the same
se = strel('disk', 5);  %create structuring element
img2 = imopen(binImg, se);%open image
compsCircle =  bwlabel(img2);%label image
B = unique(compsCircle); %check to make sure
B = B(2:size(B)); %remove zero from unique array since it doesn't indicate a component aka background
compsArea = zeros(size(B));
compsMean = zeros(size(B));

%display images
figure();
subplot(1,3,1);
imshow(binImg), title('Image');
subplot(1,3,2);
imshow(img2), title('Opened Image');
subplot(1,3,3);
RGB_circle = label2rgb(compsCircle, 'colorcube', 'k');
imshow(RGB_circle), title('Labeled Image');

%find centroid and area of circle
for i = 1:size(B)
    compsArea(i) = sum(compsCircle(:) == B(i));
    pixels = find(compsCircle == B(i));
    compsMean(i) = mean(pixels);
end

%create table structure and display
circle.region = B;
circle.area = compsArea;
circle.centroid = round(compsMean);
table_circle =struct2table(circle);
disp(table_circle);

% (ii) In this part, we are interested in performing similar operations on the binary image lines.jpg.
% Your aim now is to separate out the vertical lines from the horizontal ones in the image, and
% then calculate certain attributes corresponding to these vertical lines.
img3 = imread('data/lines.jpg');
binImg2 = imbinarize(img3(:,:,1));%all three matricies are the same

se = strel('rectangle', [10,2]);
img4 = imopen(binImg2, se);
compsLine =  bwlabel(img4);
B_line = unique(compsLine);
B_line = B_line(2:size(B_line)); %remove zero from unique array since its background
lineCentroid = zeros(size(B_line));
lineLength = zeros(size(B_line));

% Display images
figure();
subplot(1,3,1);
imshow(img3), title('Image');
subplot(1,3,2);
imshow(img4), title('Opened Image');
subplot(1,3,3);
RGB_lines = label2rgb(compsLine, 'jet', 'k');
imshow(RGB_lines), title('Labeled Image');

%Find the line centroid and max length
for i = 1:size(B_line)
    lenmax = 1;
    len = 1;
    for n = 2:numel(compsLine)
        if (compsLine(n) == compsLine(n-1)) && (compsLine(n) == B_line(i))
            len = len+1;
        else
            if len > lenmax
                lineLength(i) = len;
            end
            len = 1;
        end
    end    
    lineCentroid(i) = mean(find(compsLine == B_line(i)));
end

line.region = B_line;
line.length = lineLength;
line.centroid = round(lineCentroid);
table_line =struct2table(line);
disp(table_line);






