% Chroma keying is used for extracting the foreground from images, with the background typically
% being a green screen. In this problem, you have been provided 2 images: travolta.jpg and dog.jpg.
% Write a matlab script to extract the foreground from either image and overlay the foreground on a
% different background of your choice. In your report you should include for each of the images:
% (i) A binary image showing the foreground mask, ie., all foreground pixels set to 1 and all
% background pixels set to 0
% (ii) An image with the background pixels set to 0 and the foreground pixels set to their original
% values
% (iii) An image with the foreground overlayed on a background of your choice
clear;
close all;
imgFG = imread('data/travolta.jpg');
imgBG = imread('data/space.jpg');

%get image sizes
[bgX, bgY] = size(imgBG(:,:,1));
[fgX, fgY] = size(imgFG(:,:,1));

img_ = imgFG;
%convert to double
imgFG = im2double(imgFG);
imgBG = im2double(imgBG);

% extract 3 different color channels
% and luminance matrix
fgR = imgFG(:,:,1);
fgG = imgFG(:,:,2);
fgB = imgFG(:,:,3);
fgL = 0.3*fgR+0.59*fgG+0.11*fgB;
 
% subtract luminance from green
fgG_L=mat2gray(fgG-fgL);

%set threshold for binary mask 
thres = 90/255;

if fgX ~= bgX || fgY ~= bgY 
    % Size of B does not match A, so resize B to match A's size. 
    BG = imresize(imgBG, [fgX fgY]); 
else
    warning('Background image is too small.');
    return;
end


%create mask
mask = fgG_L < thres;

%create colored mask
rgbMask(:,:,1)=imgFG(:,:,1).*mask;
rgbMask(:,:,2)=imgFG(:,:,2).*mask;
rgbMask(:,:,3)=imgFG(:,:,3).*mask;

%Overlay on background
spaceDog(:,:,1)=rgbMask(:,:,1) + BG(:,:,1).*(1-mask);
spaceDog(:,:,2)=rgbMask(:,:,2) + BG(:,:,2).*(1-mask);
spaceDog(:,:,3)=rgbMask(:,:,3) + BG(:,:,3).*(1-mask);

figure(1);
subplot(2,2,1);
imshow(imgFG);
title('Image');
subplot(2,2,2);
imshow(mask);
title('Binary Mask');
subplot(2,2,3);
imshow(rgbMask);
title('RGB Mask');
subplot(2,2,4);
imshow(spaceDog);
title('Foreground with background');



