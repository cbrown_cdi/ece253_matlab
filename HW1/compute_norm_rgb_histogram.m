function [countsTotal] = compute_norm_rgb_histogram(img)
%COMPUTE NORM RGB HISTOGRAM.M Summary of this function goes here
% One input (RGB/color image) and one output (1 x 96 vector).
% Use 32 bins for each color channel (i.e. Red, Green and Blue), spaced equally between 0 and
% 255. This should result in a 32-length vector for each channel.
% 
% Concatenate the three histograms together (in the order R, G, B) to make a combined his-
% togram of length 3 x 32 = 96. Once you have computed the combined histogram, normalize
% it so that it sums to 1.
% Do not use MATLABs inbuilt histogram function. You may use loops if necessary.
% In your report, plot the final combined, normalized histogram for the image geisel.jpg. Make
% sure the plot is labeled correctly.
binNum = 32; 

img1 = round(rescale(img, 0, binNum-1));    %rescale to match bin number
%red
r = img1(:,:,1);
g = img1(:,:,2);
b = img1(:,:,3);

%pre-allocate
countsRed = zeros(1, binNum);
countsGreen = zeros(1, binNum);
countsBlue = zeros(1, binNum);

for i=1:binNum
    countsRed(:,i)=sum(sum(r==i));%sum of bin value in matrix
    countsGreen(:,i)=sum(sum(g==i));
    countsBlue(:,i)=sum(sum(b==i));
end

countsTotal = cat(2, countsRed, countsGreen, countsBlue);
countsTotal = countsTotal./sum(countsTotal);%create unit vector

x = 1:96;

figure(2);
bar(countsTotal);
%plot(x(1:32), countsTotal(1:32), 'r', x(32:64), countsTotal(32:64), 'g', x(64:96), countsTotal(64:96), 'b');
title('RGB Histogram');
end

