%Problem 2. Simple image manipulation (5 points)
clear; %clear all variables

% (i) Download any color image from the Internet with a spatial resolution of no more than (720
% X 480). Read this image into MATLAB. Call this image A.
A = imread('data/bike.jpg');
figure(1);
subplot(2,3,1);
imshow(A), title('Image A');

% (ii) Transform the color image to grey-scale. Verify the values are between 0 and 255. If not,
% please normalize your image from 0 to 255. Call this image B.
B = rgb2gray(A);

%check if values are between 0-255
minB = min(min(B));    %min of matrix
maxB = max(max(B));    %max of matrix
if minB > 0 || maxB < 255   %check to see if the image needs to be normalized
    disp('Grayscale image needs to be normalized.');
    B = uint8(255 .* ((double(B)-double(minB))) ./ double(maxB-minB));
    minB1 = min(min(B));    %min of matrix after normalization
    maxB1 = max(max(B));    %max of matrix
    fprintf('Original Range: %d - %d\t Normalized Range: %d - %d\n\n',minB, maxB, minB1, maxB1);
end
subplot(2,3,2);
imshow(B), title('Image B');

% (iii) Add 20 to each value of image B. Set all pixel values greater than 255 to 255. Call this image C.
C = double(B) + 20; %convert to double to add above 255
maxC1 = max(max(C));    %find the max of matrix
aboveMaxCIndices = find(C > 255);
C(aboveMaxCIndices) = 255;
C = uint8(C);   %convert back to uint8 to display

%Checking
maxC2 = max(max(C));%find the max of matrix
if maxC2 > 255
    warning('Image C: Pixel values above 255.');
    return;
else
    fprintf('(iii)Max B: %d\tMax C: %d\tMax C2: %d\n\n',maxB1, maxC1, maxC2);
end
subplot(2,3,3);
imshow(C), title('Image C');

% (iv) Flip image B along both the horizontal and vertical axis. Call this image D.
D = flip(B, 1);
D = flip(D, 2);
subplot(2,3,4);
imshow(D), title('Image D');

% (v) Calculate the median of all values in image B. Next, threshold image B by the median value
% you just calculated i.e. set all values greater than median to 1 and set all values less than or
% equal to the median to 0. Name this binary image E.
columnMedian = median(B);
totalMedian = median(columnMedian);
largePix = find(B >= totalMedian);
E = zeros(size(B));
E(largePix) = 1;
subplot(2,3,5);
imshow(E), title('Image E');
