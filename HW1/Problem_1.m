%Problem 1 MATLAB Basics
clear; %clear all variables

%Given arrays
A = [3 9 5 1; 4 25 4 3; 63 12 23 9; 6 32 77 0; 12 8 5 1;];
B = [0 1 0 1; 0 1 1 1; 0 0 0 1; 1 1 0 1; 0 1 0 0;];

disp("Problem 1");
%(i) Point-wise multiply A with B and set it to C.
C = A.*B;
disp("Array C:");
disp(C);

%(ii) Calculate the inner product of the 2nd and 5th row of C.
innerProduct = dot(C(2,:), C(5,:));
fprintf('Inner Product of C(2,:) and C(5,:): %d\n\n',innerProduct);

% (iii) Find the minimum and maximum values and their corresponding row and column indices in matrix C. If there are multiple min/max values, you must list all their indices.
minC = min(min(C));    %find the min of matrix
minIndices = find(C == minC);   %find indices of the min value
s = [5,4];
[I,J] = ind2sub(s,minIndices);
g = sprintf('%d', minC);
fprintf('Min Indices(val = %s): ', g);
for i=1:length(minIndices) %loop only used for displaying output
    g=sprintf('%d,%d ', I(i), J(i));   %print indices inline
    fprintf('%s ',g);
end
fprintf('\n\n');

maxC = max(max(C));
maxIndices = find(C == maxC);
[I,J] = ind2sub(s,maxIndices);
g = sprintf('%d', maxC);
fprintf('Max Indices(val = %s): ', g);
for i=1:length(maxIndices) %loop only used for displaying output
    g=sprintf('%d,%d ', I(i), J(i));   %print indices inline
    fprintf('%s ',g);
end
fprintf('\n\n');


